#
# Dockerfile to build Unbound DNS server image

FROM alpine:latest

RUN apk update \
    && apk --no-cache add unbound tzdata \
    && cp -v /usr/share/zoneinfo/America/New_York /etc/localtime \
    && echo "America/New_York" > /etc/timezone
    
ADD ./unbound_pihole.conf /etc/unbound/unbound.conf

EXPOSE 53/tcp
EXPOSE 53/udp

CMD /usr/sbin/unbound -d
