#!/bin/bash

case $1 in
    start)
	docker-compose -f ./compose.yml up -d
	;;
    stop)
	docker-compose -f ./compose.yml down
	;;
    man_start)
	docker run -d \
	       -p 80:80 \
	       -p 443:443 \
	       -p 53:53/tcp \
	       -p 53:53/udp \
	       -p 67:67/udp \
	       --name PiHole \
	       -h bluehole \
	       --env TZ='America/New_York' \
	       --env WEBPASSWORD='y0mama_1' \
	       --env ServerIP='172.0.0.1' \
	       -v /data/volumes/pihole-etc:/etc/pihole \
	       pihole/pihole:v5.1.2-arm64-buster
	;;
    man_stop)
	docker stop PiHole
	docker rm PiHole
	;;
    *)
	echo "wrong option, try again"
	echo "{start|stop|man_start|man_stop}"
	;;
esac
	       
